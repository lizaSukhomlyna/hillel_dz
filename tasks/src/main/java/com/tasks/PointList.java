package com.tasks;

import lombok.Getter;
import lombok.Setter;
import java.util.Arrays;

@Getter
@Setter
public class PointList {
    private Point[] arrayPoint;
    private int sizePoint = 1;

    public Point[] addPoint(Point dot) {
        if (this.arrayPoint == null) {
            arrayPoint = new Point[sizePoint];
            arrayPoint[0] = dot;
        } else {
            sizePoint++;
            Point[] copyArray = arrayPoint.clone();
            arrayPoint = new Point[arrayPoint.length + 1];
            for (int i = 0; i < copyArray.length; i++) {
                arrayPoint[i] = copyArray[i];
            }
            arrayPoint[sizePoint - 1] = dot;
            System.out.println(Arrays.toString(arrayPoint));
        }
        return arrayPoint;
    }


    public Point returnDotForIndex(int index) {
    return  this.arrayPoint[index];
    }
}