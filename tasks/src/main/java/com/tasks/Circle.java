package com.tasks;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class Circle {
    private Point center;
    private double radius;

    public boolean isDotFromCircle(Point dot) {
        return (dot.distanceTo(this.center) < this.radius);
    }
}
