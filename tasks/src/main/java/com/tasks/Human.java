package com.tasks;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Setter
@Getter
public class Human {
    private String name;
    private String surname;
    private String patronymic;

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human(String name, String surname, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;


    }

    public String getFullName() {
        if (this.patronymic != null) {
            return String.format("%s %s %s", this.surname, this.name, this.patronymic);
        } else {
            return String.format("%s %s", this.surname, this.name);
        }
    }

    public String getShortName() {
        if (this.patronymic != null) {
            return String.format("%s %s %s", this.surname, this.name.toUpperCase().charAt(1), this.patronymic.toUpperCase().charAt(1));
        } else {
            return String.format(this.surname, this.name.toUpperCase().charAt(1));
        }

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(patronymic, human.patronymic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic);
    }

    @Override
    public String toString() {
        return String.format("Human{%s %s %s}",name, surname, patronymic);
    }
}
