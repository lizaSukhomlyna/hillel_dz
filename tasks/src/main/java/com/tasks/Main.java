package com.tasks;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Выбирете номер задачи(1, 2 или 3(дз2):");
        System.out.println("Ваш выбор:");
        int numTask = in.nextInt();
        if (numTask == 1) {
            taskFirst();
        } else {
            taskSecond();
        }
        in.close();
    }



    private static void taskFirst() {
        Human person1 = new Human("Иван", "Пупкин", "Васильевич");
        Human person2 = new Human("Иван", "Иванов");
        System.out.println(person1.getFullName());
        System.out.println(person1.getShortName());
        System.out.println(person2.getFullName());
        System.out.println(person2.getShortName());
    }
    private static void taskSecond() {
        Scanner in = new Scanner(System.in);
        PointList dotList = new PointList();
        Circle circle = new Circle();
        int wishAdd = 0;
        do {
            System.out.println("Введите координаты точки");
            System.out.println("x:");
            Point dot = new Point();
            dot.setX(in.nextDouble());
            System.out.println("y:");
            dot.setY(in.nextDouble());
            dotList.addPoint(dot);
            while (wishAdd == 0) {
                System.out.println("Желаете добавить еще (1-да 2- нет)");
                System.out.println("Ваш выбор:");
                int choice = in.nextInt();
                wishAdd = (choice >= 1 && choice <= 2) ? choice : 0;
                if(wishAdd == 0)   System.out.println("Ошибка ввода!");

            }

        } while (wishAdd != 2);
        System.out.println("Введите радиус:");
        circle.setRadius(in.nextDouble());
        System.out.println("Введите координаты точки центра окружности x, y:");
        System.out.println("x:");
        double x = in.nextDouble();
        System.out.println("y:");
        double y = in.nextDouble();
        circle.setCenter(new Point(x, y));
        for (int i = 0; i < dotList.getSizePoint(); i++) {
            if ((circle.isDotFromCircle(dotList.returnDotForIndex(i)))) {
                System.out.println(String.format("(%s,%s)",
                        dotList.getArrayPoint()[i].getX(),
                        dotList.getArrayPoint()[i].getY()
                ));
                int choice = in.nextInt();
                wishAdd = (choice >= 1 && choice <= 2) ? choice : 0;
                if(wishAdd == 0)   System.out.println("Ошибка ввода!");

            }

        } while (wishAdd != 2);
        System.out.println("Введите радиус:");
        circle.setRadius(in.nextDouble());
        System.out.println("Введите координаты точки центра окружности x, y:");
        System.out.println("x:");
        double xCenter = in.nextDouble();
        System.out.println("y:");
        double yCenter = in.nextDouble();
        circle.setCenter(new Point(xCenter, yCenter));
        for (int i = 0; i < dotList.getSizePoint(); i++) {
            if ((circle.isDotFromCircle(dotList.returnDotForIndex(i)))) {
                System.out.println(String.format("(%s,%s)",
                        dotList.getArrayPoint()[i].getX(),
                        dotList.getArrayPoint()[i].getY()
                ));


            }
        }
        in.close();
    }
}
